package br.com.coelce.iomobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.interfaces.OnCheckedChangeListener;
import com.mikepenz.materialdrawer.model.SwitchDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import org.mapsforge.map.android.graphics.AndroidGraphicFactory;
import org.osmdroid.bonuspack.kml.KmlDocument;
import org.osmdroid.bonuspack.kml.KmlFeature;
import org.osmdroid.bonuspack.kml.KmlFolder;
import org.osmdroid.bonuspack.kml.KmlGeometry;
import org.osmdroid.bonuspack.kml.KmlPlacemark;
import org.osmdroid.bonuspack.kml.KmlPoint;
import org.osmdroid.bonuspack.kml.Style;
import org.osmdroid.bonuspack.location.OverpassAPIProvider;
import org.osmdroid.mapsforge.MapsForgeTileProvider;
import org.osmdroid.mapsforge.MapsForgeTileSource;
import org.osmdroid.tileprovider.MapTileProviderBasic;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.tileprovider.util.SimpleRegisterReceiver;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.FolderOverlay;
import org.osmdroid.views.overlay.ScaleBarOverlay;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import br.com.coelce.iomobile.constants.OpenStreetMapConstants;
import br.com.coelce.iomobile.drawer.ExpandableAndSwitchDrawerItem;

public class MapActivity extends AppCompatActivity implements OnCheckedChangeListener, Drawer.OnDrawerItemClickListener {

    private MapView mMapView;
    protected FolderOverlay mKmlOverlay; //root container of overlays from KML reading
    public static KmlDocument mKmlDocument; //made static to pass between activities
    public static Stack<KmlFeature> mKmlStack; //passed between activities, top is the current KmlFeature to edit.
    public static KmlFolder mKmlClipboard; //passed between activities. Folder for multiple items selection.
    private MyLocationNewOverlay mLocationOverlay;
    private Drawer result;
    private SharedPreferences mPrefs;
    private CompassOverlay mCompassOverlay;
    private List<KmlFeature> features;
    private boolean needUpdated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        assert toolbar != null;
        DrawerBuilder drawerBuilder = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .withSavedInstance(savedInstanceState)
                .withOnDrawerItemClickListener(this)
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {

                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                        if (needUpdated) {
                            needUpdated = false;
                            updateUIWithKml();
                        }
                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {

                    }
                });
        result = drawerBuilder.build();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        result.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
        mMapView = (MapView) findViewById(R.id.mapview);
//        boolean b = setMapsForgeTileProvider();
        assert mMapView != null;
        mMapView.setBuiltInZoomControls(true);
        mMapView.setMultiTouchControls(true);
        mKmlDocument = new KmlDocument();
        mKmlStack = new Stack<KmlFeature>();
        mKmlClipboard = new KmlFolder();
        features = new ArrayList<>();
        this.mLocationOverlay = new MyLocationNewOverlay(mMapView);
        mLocationOverlay.enableMyLocation();
        this.mCompassOverlay = new CompassOverlay(this, new InternalCompassOrientationProvider(this), mMapView);
        ScaleBarOverlay scaleBarOverlay = new ScaleBarOverlay(mMapView);
        mMapView.getOverlays().add(this.mLocationOverlay);
        mMapView.getOverlays().add(this.mCompassOverlay);
        mMapView.getOverlays().add(scaleBarOverlay);

        mPrefs = getSharedPreferences(OpenStreetMapConstants.PREFS_NAME, Context.MODE_PRIVATE);
        mMapView.getController().setZoom(mPrefs.getInt(OpenStreetMapConstants.PREFS_ZOOM_LEVEL, 1));
        mMapView.scrollTo(mPrefs.getInt(OpenStreetMapConstants.PREFS_SCROLL_X, 0), mPrefs.getInt(OpenStreetMapConstants.PREFS_SCROLL_Y, 0));

        //check if intent has been passed with a kml URI to load (url or file)
        String uri = getIntent().getStringExtra("io");
        if (uri != null) {
            openFile(uri, true, false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        final String tileSourceName = mPrefs.getString(OpenStreetMapConstants.PREFS_TILE_SOURCE,
                TileSourceFactory.DEFAULT_TILE_SOURCE.name());
        try {
            final ITileSource tileSource = TileSourceFactory.getTileSource(tileSourceName);
            mMapView.setTileSource(tileSource);
        } catch (final IllegalArgumentException e) {
            mMapView.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE);
        }
        if (mPrefs.getBoolean(OpenStreetMapConstants.PREFS_SHOW_LOCATION, false)) {
            this.mLocationOverlay.enableMyLocation();
        }
        if (mPrefs.getBoolean(OpenStreetMapConstants.PREFS_SHOW_COMPASS, false)) {
            this.mCompassOverlay.enableCompass();
        }
    }

    @Override
    protected void onPause() {
        final SharedPreferences.Editor edit = mPrefs.edit();
        edit.putString(OpenStreetMapConstants.PREFS_TILE_SOURCE, mMapView.getTileProvider().getTileSource().name());
        edit.putInt(OpenStreetMapConstants.PREFS_SCROLL_X, mMapView.getScrollX());
        edit.putInt(OpenStreetMapConstants.PREFS_SCROLL_Y, mMapView.getScrollY());
        edit.putInt(OpenStreetMapConstants.PREFS_ZOOM_LEVEL, mMapView.getZoomLevel());
        edit.putBoolean(OpenStreetMapConstants.PREFS_SHOW_LOCATION, mLocationOverlay.isMyLocationEnabled());
        edit.putBoolean(OpenStreetMapConstants.PREFS_SHOW_COMPASS, mCompassOverlay.isCompassEnabled());
        edit.commit();

        this.mLocationOverlay.disableMyLocation();
        this.mCompassOverlay.disableCompass();
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView =
                (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                for (KmlFeature feature : features) {
                    if (feature.mName.toLowerCase().contains(query)) {
                        mMapView.zoomToBoundingBox(feature.getBoundingBox(), true);
                        mLocationOverlay.disableFollowLocation();
                        return true;
                    }
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

//        if (mMapView.getTileProvider().getTileSource() == TileSourceFactory.MAPNIK)
//            menu.findItem(R.id.menu_tile_mapnik).setChecked(true);
//        else if (mMapView.getTileProvider().getTileSource() == MAPBOXSATELLITELABELLED)
//            menu.findItem(R.id.menu_tile_mapbox_satellite).setChecked(true);
//        mMapView.getOverlayManager().onCreateOptionsMenu(menu, Menu.FIRST + 1, mMapView);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
//        mMapView.getOverlayManager().onPrepareOptionsMenu(menu, Menu.FIRST + 1, mMapView);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if (mMapView.getOverlayManager().onOptionsItemSelected(item, Menu.FIRST + 1, mMapView))
//            return true;
        switch (item.getItemId()) {
            case R.id.menu_tile_mapnik:
                setStdTileProvider();
                mMapView.setTileSource(TileSourceFactory.MAPNIK);
                mMapView.getOverlayManager().getTilesOverlay().setColorFilter(null);
                item.setChecked(true);
                return true;
            case R.id.menu_tile_mapsforge:
                boolean result = setMapsForgeTileProvider();
                if (result)
                    item.setChecked(true);
                else
                    Toast.makeText(this, "Nenhum mapa encontrado.", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.menu_trackmode:
                if (!mLocationOverlay.isFollowLocationEnabled()) {
                    mLocationOverlay.enableFollowLocation();
                    item.setChecked(true);
                } else {
                    mLocationOverlay.disableFollowLocation();
                    item.setChecked(false);
                }
                return true;
            case R.id.menu_zoom_out:
                zoomOut();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void setStdTileProvider() {
        if (!(mMapView.getTileProvider() instanceof MapTileProviderBasic)) {
            MapTileProviderBasic bitmapProvider = new MapTileProviderBasic(this);
            mMapView.setTileProvider(bitmapProvider);
        }
    }

    boolean setMapsForgeTileProvider() {
        String path = Environment.getExternalStorageDirectory().getPath() + "/mapsforge/";
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles == null)
            return false;
        if (AndroidGraphicFactory.INSTANCE == null)
            AndroidGraphicFactory.createInstance(this.getApplication());
        MapsForgeTileProvider mfProvider = new MapsForgeTileProvider(new SimpleRegisterReceiver(this),
                MapsForgeTileSource.createFromFiles(listOfFiles));
        mMapView.setTileProvider(mfProvider);
        return true;
    }

    void openFile(String uri, boolean onCreate, boolean isOverpassRequest) {
        //Toast.makeText(this, "Loading "+uri, Toast.LENGTH_SHORT).show();
        new KmlLoadingTask("Carregando " + uri).execute(uri, onCreate, isOverpassRequest);
    }

    ProgressDialog createSpinningDialog(String title) {
        ProgressDialog pd = new ProgressDialog(mMapView.getContext());
        pd.setTitle(title);
        pd.setMessage("Aguarde");
        pd.setCancelable(false);
        pd.setIndeterminate(true);
        return pd;
    }

    @Override
    public void onCheckedChanged(IDrawerItem drawerItem, CompoundButton buttonView, boolean isChecked) {
        KmlFeature tag = (KmlFeature) drawerItem.getTag();
        tag.mVisibility = isChecked;
        if (drawerItem instanceof ExpandableAndSwitchDrawerItem) {
            List<IDrawerItem> subItems = ((ExpandableAndSwitchDrawerItem) drawerItem).getSubItems();
            for (IDrawerItem mItem : subItems) {
                if (mItem instanceof SwitchDrawerItem) {
                    ((SwitchDrawerItem) mItem).withChecked(isChecked);
                }
            }
        }
        if (tag instanceof KmlFolder) {
            ArrayList<KmlFeature> mItems = ((KmlFolder) tag).mItems;
            for (KmlFeature feature : mItems) {
                if (!(feature instanceof KmlFolder)) {
                    feature.mVisibility = isChecked;
                }
            }
        }
        needUpdated = true;
    }

    private void zoomOut() {
        BoundingBoxE6 bb = mKmlDocument.mKmlRoot.getBoundingBox();
        if (bb != null) {
            mMapView.zoomToBoundingBox(bb, true);
            mMapView.getController().setCenter(bb.getCenter());
        }
    }

    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        KmlFeature feature = (KmlFeature) drawerItem.getTag();
        mMapView.zoomToBoundingBox(feature.getBoundingBox(), true);
        mLocationOverlay.disableFollowLocation();
        return true;
    }

    class UpdateKml extends AsyncTask<Void, Void, Void> {

        ProgressDialog mPD;

        @Override
        protected void onPreExecute() {
            mPD = createSpinningDialog("Atualizando Mapa...");
            mPD.show();
            if (mKmlOverlay != null) {
                mKmlOverlay.closeAllInfoWindows();
                mMapView.getOverlays().remove(mKmlOverlay);
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            mKmlOverlay = (FolderOverlay) mKmlDocument.mKmlRoot.buildOverlay(mMapView, buildDefaultStyle(), null, mKmlDocument);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mMapView.getOverlays().add(mKmlOverlay);
            mMapView.invalidate();
            zoomOut();
            if (mPD != null)
                mPD.dismiss();
        }
    }

    class KmlLoadingTask extends AsyncTask<Object, Void, Boolean> {
        String mUri;
        boolean mOnCreate;
        ProgressDialog mPD;
        String mMessage;

        KmlLoadingTask(String message) {
            super();
            mMessage = message;
        }

        @Override
        protected void onPreExecute() {
            mPD = createSpinningDialog(mMessage);
            mPD.show();
        }

        @Override
        protected Boolean doInBackground(Object... params) {
            mUri = (String) params[0];
            mOnCreate = (Boolean) params[1];
            boolean isOverpassRequest = (Boolean) params[2];
            mKmlDocument = new KmlDocument();
            boolean ok = false;
            if (isOverpassRequest) {
                //mUri contains the query
                ok = getKMLFromOverpass(mUri);
            } else if (mUri.startsWith("file:/")) {
                mUri = mUri.substring("file:/".length());
                File file = new File(mUri);
                if (mUri.endsWith(".json"))
                    ok = mKmlDocument.parseGeoJSON(file);
                else if (mUri.endsWith(".kmz"))
                    ok = mKmlDocument.parseKMZFile(file);
                else //assume KML
                    ok = mKmlDocument.parseKMLFile(file);
            } else if (mUri.startsWith("http")) {
                ok = mKmlDocument.parseKMLUrl(mUri);
            }
            Drawable defaultKmlMarker = getResources().getDrawable(R.drawable.rl);
            Bitmap bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-RL", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.tvr_e);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-TVR-E", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.fus);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("PadFus", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.sec_enc);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-SEC-ENC", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.cpe);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-CPE", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.cpae);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-CPAE", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.pad_cd);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("PadCD", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.sec);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("PadSec", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.adv);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("PadCC", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.saida_rl);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("PadSE", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.bc);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("PadBC", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.anexo);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_Anexo_IO", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.anel);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-ANEL", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.bc);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-BC", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.cm);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-CM", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.cpr);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-CPR", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.cpr_e);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-CPR-E", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.cta);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-CTA", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.fus);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-FUS", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.fus);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-FUS-ENC", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.hib);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-HIB", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.hib_enc);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-HIB-ENC", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.rg);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-RG", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.sec);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-SEC", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.slz);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-SLZ", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.tvr);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-TVR", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.utr);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-UTR", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.utr_e);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-UTR-E", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.vcr);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-VCR", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            defaultKmlMarker = getResources().getDrawable(R.drawable.med);
            bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
            mKmlDocument.putStyle("Pad_IO-MED", new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010));
            return ok;
        }

        @Override
        protected void onPostExecute(Boolean ok) {
            if (mPD != null)
                mPD.dismiss();
            if (!ok)
                Toast.makeText(getApplicationContext(), "Sorry, unable to read " + mUri, Toast.LENGTH_SHORT).show();
            updateUIWithKml();
            ArrayList<KmlFeature> mItems = mKmlDocument.mKmlRoot.mItems;
            for (KmlFeature feature : mItems) {
                result.addItem(getDrawerItem(feature));
            }
//            if (ok) {
//                BoundingBoxE6 bb = mKmlDocument.mKmlRoot.getBoundingBox();
//                if (bb != null) {
//                    if (!mOnCreate)
//                        setViewOn(bb);
//                    else {
//                        mMapView.zoomToBoundingBox(bb, true);
////                        mMapView.getController().setCenter(bb.getCenter());
//                    }
//                }
//            }
        }
    }

    void setViewOn(BoundingBoxE6 bb) {
        if (bb != null) {
            mMapView.zoomToBoundingBox(bb, true);
        }
    }

    boolean getKMLFromOverpass(String query) {
        OverpassAPIProvider overpassProvider = new OverpassAPIProvider();
        String oUrl = overpassProvider.urlForTagSearchKml(query, mMapView.getBoundingBox(), 500, 30);
        return overpassProvider.addInKmlFolder(mKmlDocument.mKmlRoot, oUrl);
    }

    void updateUIWithKml() {
        new UpdateKml().execute((Void) null);
//        new Handler(Looper.getMainLooper()).post(new Runnable() {
//            @Override
//            public void run() {
//                if (mKmlOverlay != null) {
//                    mKmlOverlay.closeAllInfoWindows();
//                    mMapView.getOverlays().remove(mKmlOverlay);
//                }
//                mKmlOverlay = (FolderOverlay) mKmlDocument.mKmlRoot.buildOverlay(mMapView, buildDefaultStyle(), null, mKmlDocument);
//                mMapView.getOverlays().add(mKmlOverlay);
//                mMapView.invalidate();
//            }
//        });

    }

    private IDrawerItem getDrawerItem(KmlFeature feature) {
        if (feature instanceof KmlFolder) {
            ExpandableAndSwitchDrawerItem drawerItem = new ExpandableAndSwitchDrawerItem()
                    .withTag(feature)
                    .withName(feature.mName)
                    .withChecked(feature.mVisibility)
                    .withOnCheckedChangeListener(this);
            ArrayList<KmlFeature> mItems = ((KmlFolder) feature).mItems;
            for (KmlFeature mFeature : mItems) {
                drawerItem.withSubItems(getDrawerItem(mFeature));
            }
            return drawerItem;
        } else {
            KmlGeometry geometry = ((KmlPlacemark) feature).mGeometry;
            if (geometry instanceof KmlPoint) {
                features.add(feature);
            }
            IDrawerItem drawerItem = new SwitchDrawerItem()
                    .withTag(feature)
                    .withName(feature.mName)
                    .withChecked(feature.mVisibility)
                    .withOnCheckedChangeListener(this);
            return drawerItem;
        }
    }

    Style buildDefaultStyle() {
        Drawable defaultKmlMarker = getResources().getDrawable(R.drawable.marker_kml_point);
        Bitmap bitmap = ((BitmapDrawable) defaultKmlMarker).getBitmap();
        Style defaultStyle = new Style(bitmap, 0x901010AA, 3.0f, 0x20AA1010);
        return defaultStyle;
    }
}
