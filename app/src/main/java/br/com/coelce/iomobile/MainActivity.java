package br.com.coelce.iomobile;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.dexafree.materialList.listeners.RecyclerItemClickListener;
import com.dexafree.materialList.view.MaterialListAdapter;
import com.dexafree.materialList.view.MaterialListView;

import java.io.File;
import java.util.prefs.Preferences;

/**
 * A login screen that offers login via email/password.
 */
public class MainActivity extends AppCompatActivity implements RecyclerItemClickListener.OnItemClickListener {

    protected MaterialListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(mToolbar);

        SearchView searchView = (SearchView) findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(search);
        searchView.setQueryHint(getString(R.string.search));
        searchView.setIconifiedByDefault(false);
        searchView.setFocusable(false);

        mListView = (MaterialListView) findViewById(R.id.material_listview);
        assert mListView != null;
        mListView.addOnItemTouchListener(this);
        populaIO();
    }

    boolean populaIO() {
        String path = Environment.getExternalStorageDirectory().getPath() + "/io/";
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles == null)
            return false;
        for (File f : listOfFiles) {
            Card card = new Card.Builder(this)
                    .setTag(f.getAbsolutePath())
                    .withProvider(new CardProvider())
                    .setLayout(R.layout.material_small_image_card)
                    .setTitle(f.getName())
                    .setDescription(f.getAbsolutePath())
                    .endConfig()
                    .build();

            mListView.getAdapter().add(card);
        }
        return true;
    }

    private SearchView.OnQueryTextListener search = new SearchView.OnQueryTextListener() {


        @Override
        public boolean onQueryTextSubmit(String query) {
            Log.i("TESTE", query);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            Log.i("TESTE", newText);
            return false;
        }
    };


    @Override
    public void onItemClick(@NonNull Card card, int position) {
        Intent startItent = new Intent(getApplicationContext(), MapActivity.class);
        startItent.putExtra("io", "file:/" + card.getTag().toString());
        startActivity(startItent);
    }

    @Override
    public void onItemLongClick(@NonNull Card card, int position) {

    }
}

